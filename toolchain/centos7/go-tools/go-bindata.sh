#!/bin/sh
git clone https://github.com/shuLhan/go-bindata
pushd go-bindata/cmd/go-bindata
  go build -o $GOPATH/bin/go-bindata
popd
rm -rf go-bindata
