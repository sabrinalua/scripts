#!/bin/sh
wget https://dl.google.com/go/go1.12.6.linux-amd64.tar.gz
tar -C /usr/local -xvf go1.12.6.linux-amd64.tar.gz
mv /usr/local/go /usr/local/go1.12.6
rm -rf go1.12.6.linux-amd64.tar.gz
ln -s /usr/local/go1.12.6 /usr/local/go

echo 'GOROOT=/usr/local/go' >> /etc/bashrc
echo 'export PATH=$PATH:$GOROOT/bin' >> /etc/bashrc
source /etc/bashrc
