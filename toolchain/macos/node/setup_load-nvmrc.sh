#!/bin/bash

BOOTSTRAP_ASSETS=https://gitlab.com/brokkrs4ge/scripts/-/raw/master/toolchain/common_scripts
curl -o ~/.load-nvmrc $BOOTSTRAP_ASSETS/load-nvmrc

echo "source ~/.load-nvmrc" >> ~/.zshrc
source ~/.zshrc

