#!/bin/bash 


# requires homebrew
brew install pyenv

# Add pyenv initializer to shell startup script
echo 'eval "$(pyenv init -)"' >> ~/.bash_profile 

pyenv install 3.7.2
# to set python version fir directory, run 'pyenv local 3.7.2'